<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022-01-13
 * Time: 14:21
 */
namespace wenshizhengxin\wsmq;
use epii\api\result\ApiResult;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Mq
{
    private static $host=null;
    private static $port=null;
    private static $user = null;
    private static $password = null;
    private static $connection = null;
    private static $channel = null;
    /**
     * @param $host
     * @param $port
     * @param $user
     * @param $password
     */
    public static function init($host,$port,$user,$password){
        self::$host = $host;
        self::$port = $port;
        self::$user = $user;
        self::$password = $password;
    }
    public static function send(String $toUrl,int $endTime,String $callbackUrl,Array $data=[]){
        if(self::$connection==null){
            $connection = new AMQPStreamConnection(
                self::$host,
                self::$port,
                self::$user,
                self::$password
            );
            self::$connection = $connection;
            self::$channel = $connection->channel();
        }

        $msgData = [
            'toUrl'=>$toUrl,
            'endTime'=>$endTime,
            'callbackUrl'=>$callbackUrl,
            'postData'=>http_build_query($data)
        ];
        $msg = new AMQPMessage(json_encode($msgData));
        self::$channel->basic_publish($msg, 'http', 'http');
    }
    public static function getCallbackContent(){
        return new ApiResult($_POST['content']);
    }
}